package org.redhat.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.redhat.kafka.client.KafkaProducer1;
import org.redhat.kafka.client.KafkaProducer2;
import org.redhat.kafka.client.KafkaProducer3;
import org.redhat.kafka.dto.Customer;
import org.redhat.kafka.utils.CSVFileProcessor;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;

public class ProducerExample {

	public static void main(String[] args) throws Exception{
		List<Customer> inputList = CSVFileProcessor.processInputFile("src/main/resources/data1.csv");
		System.out.println("Total Records = "+ inputList.size());

		Properties properties = new Properties();
		properties.put("bootstrap.servers", "my-cluster-kafka-bootstrap-myproject.apps.mumbai-0d04.openshiftworkshop.com:443");
		properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		properties.put("security.protocol", "SSL");
		properties.put("ssl.truststore.location", "src/main/resources/keystore.jks");
		properties.put("ssl.truststore.password", "password");
		properties.put("batch.size", 30000); //16384=default value
		properties.put("linger.ms", 1);
		properties.put("buffer.memory", 33554432);

		KafkaProducer<String, String> kafkaProducer = new KafkaProducer<String, String>(properties);
		try{
			long startTime = System.nanoTime();
			//for(int i = 1; i <= 10000; i++){
			Iterator inputListIter = inputList.iterator();
			Customer customer = null;
			int i =0;
			
			while(inputListIter.hasNext()) {
				//kafkaProducer.send(new ProducerRecord<String, String>("my-topic", Integer.toString(i), "test message - " + i ));
				++i;
				customer = (Customer)inputListIter.next();
				if(customer!=null) {
					//System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++  "+i);
					kafkaProducer.send(new ProducerRecord<String, String>("my-topic", Integer.toString(i), customer.toString()+";"+i));
				}
				//System.out.println("___________________________________________________");
			}
			long endTime = System.nanoTime();
			System.out.println("Total time taken = "+ (endTime-startTime)/1000000 +" milliseconds.");
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			kafkaProducer.close();
		}

		//new Thread(new KafkaProducer1()).start();
		//new Thread(new KafkaProducer2()).start(); 
		//new Thread(new KafkaProducer3()).start(); 
	}
}
