package org.redhat.kafka.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.redhat.kafka.dto.Customer;

public class CSVFileProcessor {
	
	public static void main(String args[]) throws Exception{
		processInputFile("src/main/resources/data1.csv");
	}

	public static List<Customer> processInputFile(String inputFilePath) throws FileNotFoundException {
		List<Customer> inputList = new ArrayList<Customer>();
		BufferedReader br = null;
		try{
			File inputF = new File(inputFilePath);
			InputStream inputFS = new FileInputStream(inputF);
			br = new BufferedReader(new InputStreamReader(inputFS));
			// skip the header of the csv
			inputList = br.lines().skip(1).map(mapToItem).collect(Collectors.toList());
			//System.out.println("Total Records = "+ inputList.size());
			//System.out.println(inputList);
		} catch (IOException e) {
			System.out.println(e);
		}finally {
			if(br!=null) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return inputList ;
	}

	private static Function<String, Customer> mapToItem = (line) -> {
		String[] p = line.split(",");// a CSV has comma separated lines
		Customer customer = null;
		if(p[0] != null && p[0].trim().length() > 0 && 
		   p[1] != null && p[1].trim().length() == 10 &&
		   p[2] != null && p[2].trim().length() <= 10 &&
		   p[4] != null && p[4].trim().length() == 15) {
			customer = new Customer();
			customer.setName(p[0]);
			customer.setPancardNo(p[1]);
			customer.setDob(p[2]);
			customer.setAmount(Double.valueOf(p[3]));
			customer.setLoanAccNo(p[4]);
			customer.setLastPaymentDate(p[5]);
			customer.setOutstandingAmount(Double.valueOf(p[6]));
		}else {
			return null;
		}
		return customer;
	};
}
