package com.proj.client;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.proj.dto.Customer;
import com.proj.dto.NewCustomer;
import com.proj.util.CSVFileProcessor;

import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

public class ProducerExample {

	public static void main(String[] args) throws Exception{
long starttimeval=System.nanoTime();
		
		List<NewCustomer> inputList = CSVFileProcessor.processInputFile("src/myCSV_old.csv");
		System.out.println("Total Records = "+ inputList.size());
		long endtimeval=System.nanoTime();
		System.out.println("Total time taken valdation= "+ (endtimeval-starttimeval)/1000000 +" milliseconds.");
		
		Properties properties = new Properties();
		//properties.put("bootstrap.servers", "my-cluster-kafka-bootstrap-myproject.apps.mumbai-e1c6.openshiftworkshop.com:443");
		properties.put("bootstrap.servers", "localhost:9092");
		
		properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		//properties.put("security.protocol", "SSL");
		//properties.put("ssl.truststore.location", "src/main/resources/keystore.jks");
		//properties.put("ssl.truststore.password", "password");
		properties.put("batch.size", 30000); //16384=default value
		properties.put("linger.ms", 1);
		properties.put("buffer.memory", 33554432);

		
		System.out.println("properties:  " + properties);
		KafkaProducer<String, String> kafkaProducer = new KafkaProducer<String, String>(properties);
		ByteBuffer byteBuffer=ByteBuffer.allocate(512);
		try{
			long startTime = System.nanoTime();
			//for(int i = 1; i <= 10000; i++){
			Iterator inputListIter = inputList.iterator();
			NewCustomer c = null;
			long i =0;
			
			while(inputListIter.hasNext()) {
				//kafkaProducer.send(new ProducerRecord<String, String>("my-topic", Integer.toString(i), "test message - " + i ));
				++i;
				
				
				c = (NewCustomer)inputListIter.next();
				if(c!=null) {
					//System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++  "+i);
					/*byteBuffer.put(c.getName().getBytes());
					byteBuffer.put(c.getErrorStatus().getBytes());
					
					byteBuffer.put(c.getsNo().getBytes());
					byteBuffer.put(c.getReportingEntityId().getBytes());
					byteBuffer.put(c.getContractId().getBytes());
					byteBuffer.put(c.getPan().getBytes());
					byteBuffer.put(c.getGstin().getBytes());
					byteBuffer.put(c.getName().getBytes());
					byteBuffer.put(c.getDateofBirth().getBytes());
					byteBuffer.put(c.getMobNumber().getBytes());
					byteBuffer.put(c.getEmail().getBytes());
					byteBuffer.put(c.getCreditRating().getBytes());
					
					byteBuffer.putDouble(c.getInterestRate());
					
					
					byteBuffer.put(c.getErrorStatus().getBytes());*/
					
					
					kafkaProducer.send(new ProducerRecord<String, String>("my-topic", Long.toString(i), c.toString()+";"+i));
					
					//kafkaProducer.send(new ProducerRecord<Long, ByteBuffer>("test04", Long.valueOf(i), byteBuffer));
					
				
				}
				
				//System.out.println("___________________________________________________");
			}
			long endTime = System.nanoTime();
			System.out.println("Total time taken = "+ (endTime-startTime)/1000000 +" milliseconds.");
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			kafkaProducer.close();
		}

		//new Thread(new KafkaProducer1()).start();
		//new Thread(new KafkaProducer2()).start(); 
		//new Thread(new KafkaProducer3()).start(); 
	}
}
