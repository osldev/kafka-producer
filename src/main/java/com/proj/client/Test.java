package com.proj.client;

import java.io.*;

class Globals
{
  final int INVALID = -1;
  //final int MAXMEMBER = 100;
  int MAXORDERS = 40000000;
  final int MAXTICKSPERSTOCK = 10000;
  final int MAXINTSFORPRICE = 32;
  final int MAXMASKS = 32;
  //final int BITSPERINT = 32;

  //final int ORDER = 1;
  //final int TRADE = 2;

  final int BUY = 1;
 // final int SELL = 2;

  final int INFINITE = 10000000;
  final int MAXHEAPSIZE = 20001;  //numbering starts from 1 
  final int MAXBUFSIZE = 1000000;

  public int sellpriceheap[]= new int[MAXHEAPSIZE]; // min heap 
  public int buypriceheap[] = new int[MAXHEAPSIZE]; // max heap 
  public int direction[] = new int[MAXORDERS];
  public int sellpriceheapsize, buypriceheapsize;
  public int numtrades;
  //public int buyorderpricebits[] = new int[MAXINTSFORPRICE];
  //public int sellorderpricebits[] = new int[MAXINTSFORPRICE];
  //public int bitmask[] = new int[MAXMASKS];
  //public int revbitmask[] = new int[MAXMASKS];

  //public char logbuffer[] = new char[MAXBUFSIZE];
class Ordernode
  {
     int memberid;
     int orderid;
     int securityid;
     int quantity;
     int price;
     int nextorder;
     int prevorder;
     Ordernode()
     {
     }
  }

  Ordernode[] buyorderarray = new Ordernode[MAXORDERS];
  Ordernode[] sellorderarray = new Ordernode[MAXORDERS];

  class Pricenode
  {
     int price; 
     int totalorders;
     int orderlisthead;
     int orderlisttail;
     Pricenode()
     {
         price = INVALID;
         totalorders = 0;
         orderlisthead = INVALID;
         orderlisttail = INVALID;
     }
  }

  Pricenode[] buypricearray = new Pricenode[MAXTICKSPERSTOCK];
  Pricenode[] sellpricearray = new Pricenode[MAXTICKSPERSTOCK];

  Globals()
  {
     int i;
     BufferedReader insert = new BufferedReader(new InputStreamReader(System.in));
     String str;
     while(true)
     {
        try
        {
           System.out.println("Enter the number of Max orders for Application ");
           str =  insert.readLine();
           break;
        }
        catch(Exception e)
        {
           System.out.println("Encountered an exception : "+e);
           System.out.println("Try again please");
           continue;
        }
     }
     MAXORDERS = Integer.parseInt(str);
     
     //System.out.println("Inside Globals constructor" + MAXORDERS);
     for (i=0; i < MAXTICKSPERSTOCK; i++)
     {
           buypricearray[i] = new Pricenode();
           sellpricearray[i] = new Pricenode();
     }

     for (i=0; i < MAXORDERS; i++)
     {
          buyorderarray[i] = new Ordernode();
          sellorderarray[i] = new Ordernode();
     }

     sellpriceheapsize = 0;
     sellpriceheap[0] = INVALID;

     buypriceheapsize = 0;
     buypriceheap[0] = INFINITE;
     numtrades = 0;
  }
}

class Control
{
  void processBuyOrder(Globals handle, int orderid, int securityid, int price, int quantity)
  {
     int lowestsellprice, remainingbuyquantity, nextorderid, sellorderquantity;
     int i;
     boolean done = false;
     remainingbuyquantity = quantity;
     do
     {
        lowestsellprice = sellPriceHeapMin(handle);
        if ((price < lowestsellprice) || (lowestsellprice < 0))
        {
           insertBuyOrder(handle, orderid, price, remainingbuyquantity);
           i = price;
           if (handle.buypricearray[i].totalorders == 1)
           buyPriceHeapInsert(handle, price);
           done = true;
        }
        else
        { 
           i = lowestsellprice;
           nextorderid = handle.sellpricearray[i].orderlisthead;
        if (nextorderid == handle.INVALID)
           {
              System.out.println("impossible event - sell in heap but not in array");
              System.exit (1);
           }
           sellorderquantity = handle.sellorderarray[nextorderid].quantity;
           if (sellorderquantity <= remainingbuyquantity)
           {
              handle.numtrades++;
              removeSellOrder(handle, nextorderid);
              if (handle.sellpricearray[i].totalorders == 0)
                 sellPriceHeapRemoveMin(handle);
              remainingbuyquantity = remainingbuyquantity - sellorderquantity;
              if (remainingbuyquantity == 0)
                 done = true;
           }
           else
           {
              handle.numtrades++;
              handle.sellorderarray[nextorderid].quantity = handle.sellorderarray[nextorderid].quantity - remainingbuyquantity;
              remainingbuyquantity = 0;
              done = true;
           }
        }
     }while (done == false);
  }

  void processSellOrder(Globals handle,int orderid, int securityid, int price, int quantity)
  {
     int highestbuyprice, remainingsellquantity, nextorderid, buyorderquantity;
     int i;
     boolean done = false;
     remainingsellquantity = quantity;
     do
     {
        highestbuyprice = buyPriceHeapMax(handle);
        if ((price > highestbuyprice) || (highestbuyprice < 0))
        {
           insertSellOrder(handle, orderid, price, remainingsellquantity);
           i = price;
           if (handle.sellpricearray[i].totalorders == 1)
              sellPriceHeapInsert(handle, price);
           done = true;
        }
        else
        { 
           i = highestbuyprice;
           nextorderid = handle.buypricearray[i].orderlisthead;
           if (nextorderid == handle.INVALID)
           {
              System.out.println("impossible event - buy in heap but not in array");
              System.exit(1);
           }
           buyorderquantity = handle.buyorderarray[nextorderid].quantity;
           if (buyorderquantity <= remainingsellquantity)
           {
              handle.numtrades++;
              removeBuyOrder(handle, nextorderid);
              if (handle.buypricearray[i].totalorders == 0)
                 buyPriceHeapRemoveMax(handle);
              remainingsellquantity = remainingsellquantity - buyorderquantity;
              if (remainingsellquantity == 0)
                 done = true;
           }
           else
           {
               handle.numtrades++;
               handle.buyorderarray[nextorderid].quantity = handle.buyorderarray[nextorderid].quantity - remainingsellquantity;
               remainingsellquantity = 0;
               done = true;
           }
        }
     }while (done == false);
  }

  void insertBuyOrder(Globals handle, int orderid, int price, int quantity)
  {
     int i, temp;

     handle.buyorderarray[orderid].orderid = orderid;
     handle.buyorderarray[orderid].price = price;
     handle.buyorderarray[orderid].quantity = quantity;
     handle.buyorderarray[orderid].prevorder = handle.INVALID;
     handle.buyorderarray[orderid].nextorder = handle.INVALID;

     i = price;
     handle.buypricearray[i].price = price;
     if (handle.buypricearray[i].orderlisttail != handle.INVALID)
     {
temp = handle.buypricearray[i].orderlisttail;
        handle.buyorderarray[temp].nextorder = handle.buyorderarray[orderid].orderid;
        handle.buyorderarray[orderid].prevorder = handle.buyorderarray[temp].orderid;
     }
     else
        handle.buypricearray[i].orderlisthead = handle.buyorderarray[orderid].orderid;
     handle.buypricearray[i].orderlisttail = handle.buyorderarray[orderid].orderid;
     handle.buypricearray[i].totalorders++;
  }

  void insertSellOrder(Globals handle, int orderid, int price, int quantity)
  {
     int i, temp;

     handle.sellorderarray[orderid].orderid = orderid;
     handle.sellorderarray[orderid].price = price;
     handle.sellorderarray[orderid].quantity = quantity;
     handle.sellorderarray[orderid].prevorder = handle.INVALID;
     handle.sellorderarray[orderid].nextorder = handle.INVALID;

     i = price;
     handle.sellpricearray[i].price = price;
     if (handle.sellpricearray[i].orderlisttail != handle.INVALID)
     {
        temp = handle.sellpricearray[i].orderlisttail;
        handle.sellorderarray[temp].nextorder = handle.sellorderarray[orderid].orderid;
        handle.sellorderarray[orderid].prevorder = handle.sellorderarray[temp].orderid;
     }
     else
        handle.sellpricearray[i].orderlisthead = handle.sellorderarray[orderid].orderid;
     handle.sellpricearray[i].orderlisttail = handle.sellorderarray[orderid].orderid;
     handle.sellpricearray[i].totalorders++;
  }

  void removeBuyOrder(Globals handle, int orderid)
  {
     int i, temp;
     i = handle.buyorderarray[orderid].price;
     handle.buypricearray[i].totalorders--;
     if(handle.buyorderarray[orderid].prevorder != handle.INVALID)
     {
        temp = handle.buyorderarray[orderid].prevorder;
        handle.buyorderarray[temp].nextorder = handle.buyorderarray[orderid].nextorder;
     if(orderid == handle.buypricearray[i].orderlisttail)
        {
           handle.buypricearray[i].orderlisttail = handle.buyorderarray[orderid].prevorder;
        }
     }
     else
     {
        handle.buypricearray[i].orderlisthead = handle.buyorderarray[orderid].nextorder;
        if(handle.buypricearray[i].orderlisthead == handle.INVALID)
        {
           handle.buypricearray[i].orderlisttail = handle.INVALID;
        }
     }
     if(handle.buyorderarray[orderid].nextorder != handle.INVALID)
     {
        temp = handle.buyorderarray[orderid].nextorder;
        handle.buyorderarray[temp].prevorder = handle.buyorderarray[orderid].prevorder;
     }
     handle.buyorderarray[orderid].quantity = handle.buyorderarray[orderid].price = handle.buyorderarray[orderid].nextorder = handle.buyorderarray[orderid].prevorder = handle.INVALID;
  }

  void removeSellOrder(Globals handle, int orderid)
  {
     int i, temp;
     i = handle.sellorderarray[orderid].price;
     handle.sellpricearray[i].totalorders--;
     if(handle.sellorderarray[orderid].prevorder != handle.INVALID)
     {
        temp = handle.sellorderarray[orderid].prevorder;
        handle.sellorderarray[temp].nextorder = handle.sellorderarray[orderid].nextorder;
        if(orderid == handle.sellpricearray[i].orderlisttail)
        {
           handle.sellpricearray[i].orderlisttail = handle.sellorderarray[orderid].prevorder;
        }
     }
     else
     {
        handle.sellpricearray[i].orderlisthead = handle.sellorderarray[orderid].nextorder;
        if(handle.sellpricearray[i].orderlisthead == handle.INVALID)
        {
           handle.sellpricearray[i].orderlisttail = handle.INVALID;
        }
     }
     if(handle.sellorderarray[orderid].nextorder != handle.INVALID)
     {
        temp = handle.sellorderarray[orderid].nextorder;
        handle.sellorderarray[temp].prevorder = handle.sellorderarray[orderid].prevorder;
     }
     handle.sellorderarray[orderid].quantity = handle.sellorderarray[orderid].price = handle.sellorderarray[orderid].nextorder = handle.sellorderarray[orderid].prevorder = handle.INVALID;
  }

  int sellPriceHeapInsert(Globals handle, int value)
  {
     int i, j;
     int temp;

     handle.sellpriceheapsize++;
     if (handle.sellpriceheapsize == handle.MAXHEAPSIZE)
     {
        System.out.println("Sorry sell price heap exceeded max heap size of" + handle.MAXHEAPSIZE);
        System.exit(1);
     }
     i = handle.sellpriceheapsize;
     handle.sellpriceheap[i] = value;
     j = i / 2;
     while (handle.sellpriceheap[j] > handle.sellpriceheap[i])
     {
        temp = handle.sellpriceheap[j];
        handle.sellpriceheap[j] = handle.sellpriceheap[i];
        handle.sellpriceheap[i] = temp;
        i = j;
        j = i / 2;
     }
     return 1;
  }
  /* 
  void print(Globals handle)
  {
     for(int i = 0; i< handle.buypriceheapsize; i++)
        System.out.print(" "+handle.buypriceheap[i] + " ");
  }
  */

  int buyPriceHeapInsert(Globals handle, int value)
  {
     int i, j;
     int temp;

     handle.buypriceheapsize++;
     if (handle.buypriceheapsize == handle.MAXHEAPSIZE)
     {
        System.out.println("Sorry sell price heap exceeded max heap size of" + handle.MAXHEAPSIZE);
        System.exit(1);
     }
     i = handle.buypriceheapsize;
     handle.buypriceheap[i] = value;
     j = i / 2;
     while (handle.buypriceheap[j] > handle.buypriceheap[i])
     {
        temp = handle.sellpriceheap[j];
        handle.buypriceheap[j] = handle.buypriceheap[i];
        handle.buypriceheap[i] = temp;
        i = j;
        j = i / 2;
     }
     return 1;
  }

  int sellPriceHeapRemoveMin(Globals handle)
  {
     int retval;
     int i, j, k, temp;
     int minindex, minval;
     boolean done;

      retval = handle.sellpriceheap[1];
      handle.sellpriceheap[1] = handle.sellpriceheap[handle.sellpriceheapsize];
      handle.sellpriceheapsize--;
      i = 1;
      done = false;
      do
      {
          j = 2*i;
          k = j+1;
          if (j <= handle.sellpriceheapsize)
          {
            minval = handle.sellpriceheap[j];
            minindex = j;
            if (k <= handle.sellpriceheapsize)
            {
              if (handle.sellpriceheap[k] < minval)
              {
                 minval = handle.sellpriceheap[k];
                 minindex = k;
              }
            }
            if (minval < handle.sellpriceheap[i])
            {
               temp = handle.sellpriceheap[i];
               handle.sellpriceheap[i] = minval;
               handle.sellpriceheap[minindex] = temp;
               i = minindex;
            }
            else
               done = true;
          }
          else
            done = true;
     } while (done == false);
     return retval;
  }

  int sellPriceHeapMin(Globals handle)
  {
       if (handle.sellpriceheapsize == 0)
          return handle.INVALID;

       return handle.sellpriceheap[1];
  }

  int buyPriceHeapRemoveMax(Globals handle)
  {
     int retval;
     int i, j, k, temp;
     int maxindex, maxval;
     boolean done;

      retval = handle.buypriceheap[1];
      handle.buypriceheap[1] = handle.buypriceheap[handle.buypriceheapsize];
      handle.buypriceheapsize--;
      i = 1;
      done = false;
      do
      {
          j = 2*i; k = j+1;
          if (j <= handle.buypriceheapsize)
          {
            maxval = handle.buypriceheap[j];
            maxindex = j;
            if (k <= handle.buypriceheapsize)
            {
              if (handle.buypriceheap[k] > maxval)
              {
                 maxval = handle.buypriceheap[k];
                 maxindex = k;
              }
            }
            if (maxval > handle.buypriceheap[i])
            {
              temp = handle.buypriceheap[i];
              handle.buypriceheap[i] = maxval;
              handle.buypriceheap[maxindex] = temp;
              i = maxindex;
            }
            else
               done = true;
          }
          else
            done = true;
     } while (done == false);
     return retval;
  }

  int buyPriceHeapMax(Globals handle)
  {
       if (handle.buypriceheapsize == 0)
               return handle.INVALID;

       return handle.buypriceheap[1];
  }
  Control()
  {
     //System.out.println("Control Object created");
  }

}

class Matcher
{
  public static void main(String[] args)
  {
     Globals handle = new Globals();
     Control callMe = new Control();

     //BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
     double timetaken;
     int i;
     double rate;
     int rprice, rside, rqty;
     int ordernum = 0; //, buyOrders = 0, sellOrders = 0;
     int rsec;
     int numbuy = 0, numsell = 0;
     long startTime, endTime, totalTime;
     /*String str;
     while(true)
     {
        try
        { 
           System.out.println("Enter the number of Max orders for Application ");
           str =  input.readLine();
           break;
        }
        catch(Exception e)
        {
           System.out.println("Encountered an exception : "+e);
           System.out.println("Try again please");
           continue;
        }
     }
     handle.MAXORDERS = Integer.parseInt(str);
     int direction[] = new int[handle.MAXORDERS];
     */
     for (i=0; i < handle.MAXORDERS; i++)
     {
        rsec = (int) (Math.random() *100 + 1);
        rprice =  (int) (Math.random() *500 + 1 );
        rside = (int) (Math.random() *2 + 1);
        rqty = (int) (Math.random() *1000 + 1);
        handle.direction[i] = rside;
        if (rside == handle.BUY)
        {
               numbuy++;
               handle.buyorderarray[ordernum].securityid = rsec;
               handle.buyorderarray[ordernum].price = rprice;
               handle.buyorderarray[ordernum].quantity = rqty;
               //buyOrders++;
       }
       else {
               numsell++;
               handle.sellorderarray[ordernum].securityid = rsec;
               handle.sellorderarray[ordernum].price = rprice;
               handle.sellorderarray[ordernum].quantity = rqty;
               //sellOrders++;
       }
       ordernum++;
     }
     //startTime = System.nanoTime();
      startTime = System.currentTimeMillis();

     for (i=0; i < handle.MAXORDERS; i++)
     {
        rside = handle.direction[i];
        if (rside == 1)
        {
           rsec = handle.buyorderarray[i].securityid;
           rprice = handle.buyorderarray[i].price;
           rqty = handle.buyorderarray[i].quantity;
           callMe.processBuyOrder(handle, i, rsec, rprice, rqty);
        }
       else
       {
          rsec = handle.sellorderarray[i].securityid;
          rprice = handle.sellorderarray[i].price;
          rqty = handle.sellorderarray[i].quantity;
          callMe.processSellOrder(handle, i, rsec, rprice, rqty);
       }
     }
     //System.out.println("number of orders "+ordernum);
     //endTime = System.nanoTime();
     endTime = System.currentTimeMillis();

     totalTime = endTime - startTime;
     timetaken = (double)(totalTime);//000000;
     rate = (double)ordernum;
     rate = rate / timetaken;
     rate = rate / 1000;
     
     //System.out.println("totaltime: " + totalTime); 
     System.out.println("\n----------------------");
     System.out.println("Number of orders processed =" + ordernum + "in" + timetaken /(double)1000.0 + "seconds");
     System.out.println("Number of buy orders =" +numbuy+ ", Number of sell orders =" + numsell);
     System.out.println("Number of trades generated =" +  handle.numtrades);
     System.out.println("ORDER RATE =" + rate + " MILLION ORDERS/SEC");
     //System.out.println("Average Execution Time per Order =" + (timetaken * 1000000)/(double)ordernum + " micro seconds");
     System.out.println("Average Execution Time per Order =" + (timetaken * 1000)/(double)ordernum + " micro seconds");
     System.out.println("----------------------\n\n");
     //callMe.print(handle);
  }
}
