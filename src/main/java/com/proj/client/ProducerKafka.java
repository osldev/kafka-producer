package com.proj.client;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.redhat.kafka.dto.Customer;
import org.redhat.kafka.utils.CSVFileProcessor;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.DescribeTopicsResult;
import org.apache.kafka.clients.admin.ListTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;

import com.proj.util.KafkaCSVFileProcessor;
import com.proj.util.KafkaCSVFileProcessorString;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

public class ProducerKafka {

	public static void main(String[] args) throws Exception {
		
		
		long starttimeval = System.nanoTime();
		System.out.println("Start Time = " + System.currentTimeMillis());
		//System.out.println("System.getProperty:  " + System.getProperty("partn"));
		List<Customer> inputList = CSVFileProcessor.processInputFile("src/main/resources/data1.csv");
			
		// KafkaCSVFileProcessorString.processInputFile("D:\\PCR\\myCSV_50K_"+System.getProperty("partn")+".csv");
		//List<String> inputList = KafkaCSVFileProcessorString
		//KafkaCSVFileProcessorString.processInputFile("/home/ec2-user/kafka-prod-updated-1/myCSV_50K_"+System.getProperty("partn")+".csv");
	    System.out.println("Total Records = " + inputList.size());
		//long endtimeval = System.nanoTime();
		//System.out.println("Total time taken validation= " + (endtimeval - starttimeval) / 1000000 + " milliseconds.");

		Properties properties = new Properties();
		properties.put("bootstrap.servers","my-cluster-kafka-bootstrap-myproject.apps.mumbai-0d04.openshiftworkshop.com:443");
		//properties.put("bootstrap.servers", "localhost:9092");

		properties.put("key.serializer", "org.apache.kafka.common.serialization.LongSerializer");
		properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

		properties.put("security.protocol", "SSL");
		//properties.put("ssl.truststore.location", "/home/ec2-user/kafka-prod-updated-1/keystore.jks");
		properties.put("ssl.truststore.location", "src/main/resources/keystore.jks");
		properties.put("ssl.truststore.password", "password");
		// properties.put("security.protocol", "SSL");
		// properties.put("ssl.truststore.location",
		// "src/main/resources/keystore.jks");
		// properties.put("ssl.truststore.password", "password");
		properties.put("batch.size", 30000); // 16384=default value
		properties.put("linger.ms", 1);
		properties.put("buffer.memory", 33554432);

		//String topic = "testtopic"+System.getProperty("partn");
          
		String topic = "my-topic";
		 
		KafkaProducer<Long, String> kafkaProducer = new KafkaProducer<Long, String>(properties);
		try {
			long startTime = System.nanoTime();
			Iterator inputListIter = inputList.iterator();
			long i = 0;
			while (inputListIter.hasNext()) {
				++i;
				Customer s = (Customer) inputListIter.next();
				if(s==null)continue;
				//kafkaProducer.send(new ProducerRecord<Long, String>(topic, i, s));
				kafkaProducer.send(new ProducerRecord<Long, String>("my-topic", i, s.toString()+";"+i));
				
			}
			long endTime = System.nanoTime();
			System.out.println("Total time taken = " + (endTime - startTime) / 1000000 + " milliseconds.");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			kafkaProducer.close();
		}

		// new Thread(new KafkaProducer1()).start();
		// new Thread(new KafkaProducer2()).start();
		// new Thread(new KafkaProducer3()).start();
	}
}
