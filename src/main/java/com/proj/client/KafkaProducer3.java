package com.proj.client;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class KafkaProducer3 implements Runnable{

	public void run() {
		Properties properties = new Properties();
        properties.put("bootstrap.servers", "my-cluster-kafka-bootstrap-myproject.apps.mumbai-e1c6.openshiftworkshop.com:443");
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("security.protocol", "SSL");
        properties.put("ssl.truststore.location", "src/main/resources/keystore.jks");
        properties.put("ssl.truststore.password", "password");
        properties.put("batch.size", 1638400); //16384=default value
        properties.put("linger.ms", 5);
        properties.put("buffer.memory", 33554432);
        
        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<String, String>(properties);
        try{
        	long startTime = System.nanoTime();
            for(int i = 20001; i <= 30000; i++){
                System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++  "+i);
                kafkaProducer.send(new ProducerRecord<String, String>("my-topic", Integer.toString(i), "test message - " + i ));
                System.out.println("___________________________________________________");
            }
            long endTime = System.nanoTime();
            System.out.println("KafkaProducer3 Total time taken = "+ (endTime-startTime)/1000000000 +" seconds.");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            kafkaProducer.close();
        }
		
	}

}
