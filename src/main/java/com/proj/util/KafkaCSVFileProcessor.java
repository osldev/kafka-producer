package com.proj.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.proj.dto.Cust;
import com.proj.dto.Customer;
import com.proj.dto.NewCustomer;



public class KafkaCSVFileProcessor {
	
	public static void main(String args[]) throws Exception{
		processInputFile("src/main/resources/data1.csv");
	}

	public static List<Cust> processInputFile(String inputFilePath) throws FileNotFoundException {
		List<Cust> inputList = new ArrayList<Cust>();
		BufferedReader br = null;
		try{
			File inputF = new File(inputFilePath);
			InputStream inputFS = new FileInputStream(inputF);
			br = new BufferedReader(new InputStreamReader(inputFS));
			// skip the header of the csv
			
			
			inputList = br.lines().skip(1).map(mapToItem).collect(Collectors.toList());
			//System.out.println("Total Records = "+ inputList.size());
			//System.out.println(inputList);
		} catch (IOException e) {
			System.out.println(e);
		}finally {
			if(br!=null) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return inputList ;
	}

	private static Function<String, Cust> mapToItem = (line) -> {
		Cust cust=new Cust();
		cust.setData(line);
		return cust;
	};
}
