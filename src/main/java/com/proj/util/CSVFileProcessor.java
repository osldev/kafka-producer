package com.proj.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.proj.dto.Customer;
import com.proj.dto.NewCustomer;



public class CSVFileProcessor {
	
	public static void main(String args[]) throws Exception{
		processInputFile("src/main/resources/data1.csv");
	}

	public static List<NewCustomer> processInputFile(String inputFilePath) throws FileNotFoundException {
		List<NewCustomer> inputList = new ArrayList<NewCustomer>();
		BufferedReader br = null;
		try{
			File inputF = new File(inputFilePath);
			InputStream inputFS = new FileInputStream(inputF);
			br = new BufferedReader(new InputStreamReader(inputFS));
			// skip the header of the csv
			inputList = br.lines().skip(1).map(mapToItem).collect(Collectors.toList());
			//System.out.println("Total Records = "+ inputList.size());
			//System.out.println(inputList);
		} catch (IOException e) {
			System.out.println(e);
		}finally {
			if(br!=null) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return inputList ;
	}

	private static Function<String, NewCustomer> mapToItem = (line) -> {
	
		Pattern panPattern = Pattern.compile("[A-Z]{5}[0-9]{4}[A-Z]{1}");
		Pattern gstPattern=  Pattern.compile("([0-2][1-9]|[1-2][0-9]|3[0-5])[A-Z0-9]{10}[1-9][Z][A-Z0-9]");
		/*Pattern intPattern= Pattern.compile("^\\d*(\\.\\d+)?$");
	
		Pattern namepattern = Pattern.compile("^[a-zA-Z0-9\\s]*$");*/
		line=line.replaceAll("\"", "");
		boolean noError=false;
		String[] p = line.split(",");// a CSV has comma separated lines
		NewCustomer customer = null;
		long starttimeval=System.nanoTime();
		if(p[2] != null && p[2].trim().length() > 0 &&
				   p[3] != null && p[3].trim().length() == 10 && panPattern.matcher(p[3]).matches() &&
				   p[5] != null && p[5].trim().length() > 0 &&
				   p[6] != null && p[6].trim().length() > 0 &&
				   p[7] != null && p[7].trim().length() == 10 &&
				   p[8] != null && p[8].trim().length() > 0 &&
				   p[75] != null && Double.parseDouble( p[75]) > 0 &&
				   p[9] != null && p[9].trim().length() > 0 &&(p[9].equals("AA") || p[9].equals("BB") || p[9].equals("AAA") || p[9].equals("BBB") || p[9].equals("CC"))
				   &&((p[4]!=null && p[4].trim().length()==15  && p[4].substring(2,12).equals(p[3]) && gstPattern.matcher(p[4]).matches()) || p[4]==null)
				   ) {
						
							try{
								noError=true;
								/*Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(p[6]);
								Date date2=new SimpleDateFormat("dd/MM/yyyy").parse(new SimpleDateFormat("dd/MM/yyyy").format( new Date()));
								if(date1.compareTo(date2)<0){
									noError=true;
									if(p[4]!=null){
										if(p[4].trim().length()==15 && gstPattern.matcher(p[4]).matches() && p[4].substring(2,12).equals(p[3])){
											
											noError=true;	
										}
									}else{
										noError=true;
									}
									
								}*/
							}catch(NumberFormatException e){
								noError=false;
								p[75]="0";
								e.printStackTrace();
							} 
							
						
						
				}
		long endtimeval=System.nanoTime();
		System.out.println("Total time per record  taken valdation= "+ (endtimeval-starttimeval)+" nanoseconds.");
				if(noError){
					NewCustomer c=new NewCustomer();
					c.setsNo((p[0]==null || p[0].equals(""))?"":p[0]);
					c.setReportingEntityId((p[1]==null || p[1].equals(""))?"":p[1]);
					c.setContractId((p[2]==null || p[2].equals(""))?"":p[2]);
					c.setPan((p[3]==null || p[3].equals(""))?"":p[3]);
					c.setGstin((p[4]==null || p[4].equals(""))?"":p[4]);
					c.setName((p[5]==null || p[5].equals(""))?"":p[5]);
					c.setDateofBirth((p[6]==null || p[6].equals(""))?"":p[6]);
					c.setMobNumber((p[7]==null || p[7].equals(""))?"":p[7]);
					c.setEmail((p[8]==null || p[8].equals(""))?"":p[8]);
					c.setCreditRating((p[9]==null || p[9].equals(""))?"":p[9]);
					
					c.setInterestRate(Double.parseDouble(p[75]));
					
					
					
					c.setErrorStatus("N");
					customer=c;
				}else{
					System.out.println("error:  "+ p[0]);
					NewCustomer c=new NewCustomer();
					c.setsNo((p[0]==null || p[0].equals(""))?"":p[0]);
					c.setReportingEntityId((p[1]==null || p[1].equals(""))?"":p[1]);
					c.setContractId((p[2]==null || p[2].equals(""))?"":p[2]);
					c.setPan((p[3]==null || p[3].equals(""))?"":p[3]);
					c.setGstin((p[4]==null || p[4].equals(""))?"":p[4]);
					c.setName((p[5]==null || p[5].equals(""))?"":p[5]);
					c.setDateofBirth((p[6]==null || p[6].equals(""))?"":p[6]);
					c.setMobNumber((p[7]==null || p[7].equals(""))?"":p[7]);
					c.setEmail((p[8]==null || p[8].equals(""))?"":p[8]);
					c.setCreditRating((p[9]==null || p[9].equals(""))?"":p[9]);
					
					c.setInterestRate(Double.parseDouble(p[75]));
					
					
					c.setErrorStatus("Y");
					customer=c;
				}
		return customer;
	};
}
