package com.proj.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.proj.dto.Cust;
import com.proj.dto.Customer;
import com.proj.dto.NewCustomer;



public class KafkaCSVFileProcessorString {
	
	public static void main(String args[]) throws Exception{
		processInputFile("src/main/resources/data1.csv");
	}

	public static List<String> processInputFile(String inputFilePath) throws FileNotFoundException {
		List<String> inputList = new LinkedList<String>();
		BufferedReader br = null;
		try{
			
			  /*Scanner scanner = new Scanner(new File(inputFilePath));
		        while (scanner.hasNext()) {
		            String line = scanner.nextLine();
		            inputList.add(line);
		            
		        }
		        scanner.close();*/
			/*File inputF = new File(inputFilePath);
			
			byte [] fileBytes = Files.readAllBytes(inputF.toPath());
		    char singleChar;
		    for(byte b : fileBytes) {
		      singleChar = (char) b;
		    }
		    */
		    
			InputStream inputFS = new FileInputStream(inputFilePath);
			br = new BufferedReader(new InputStreamReader(inputFS));
			 String line =null;
			 int i=0;
		        while((line=br.readLine())!=null){
		        	if(i==0){
		        		
		        	}else{
		        	inputList.add(line);
		        	}i++;
		            }
		       
		       // inputList = br.lines().collect(Collectors.toList());
			//inputList = br.lines().skip(1).map(s).collect(Collectors.toList());
			//System.out.println("Total Records = "+ inputList.size());
			//System.out.println(inputList);
		} catch (IOException e) {
			System.out.println(e);
		}finally {
			if(br!=null) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return inputList ;
	}

	private static Function<String, Cust> mapToItem = (line) -> {
		Cust cust=new Cust();
		cust.setData(line);
		return cust;
	};
}
