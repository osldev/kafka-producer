package com.proj.util;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
 
public class ReadFile_Files_ReadAllBytes {
  public static void main(String [] pArgs) throws IOException {
	  long startTime=System.nanoTime();
    String fileName = "src/myCSV_old.csv";
    File file = new File(fileName);
 
    byte [] fileBytes = Files.readAllBytes(file.toPath());
    char singleChar;
    for(byte b : fileBytes) {
      singleChar = (char) b;
      
    }
    long endTime = System.nanoTime();
	System.out.println("Total time taken = "+ (endTime-startTime)/1000000 +" milliseconds.");
  }
}